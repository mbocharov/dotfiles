set nocompatible               " be improved, required
filetype off                   " required
" set the runtime path to include Vundle and initialize
set rtp+=~/.config/nvim/bundle/Vundle.vim
call vundle#begin()            " required
Plugin 'VundleVim/Vundle.vim'  " required
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'scrooloose/nerdtree'
Plugin 'powerman/vim-plugin-ruscmd'
Plugin 'Raimondi/delimitMate'
Plugin 'dracula/vim'
"Plugin 'Shougo/deoplete.nvim'
Plugin 'Valloric/YouCompleteMe'
" ===================
" my plugins here
" ===================

" Plugin 'dracula/vim'


" ===================
" end of plugins
" ===================
call vundle#end()               " required
filetype plugin indent on       " required
" ===================
" plugins settings
" ===================
" nerdtree
map <C-n> :NERDTreeToggle<CR>
" vim-syntax
" ===================
" base settings
" ===================
" tab-spaces
set tabstop=4
set softtabstop=4
set shiftwidth=4
set noexpandtab
set colorcolumn=110
highlight ColorColumn ctermbg=darkgray
colorscheme dracula
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
map  <C-l> :tabn<CR>
map  <C-h> :tabp<CR>
let NERDTreeQuitOnOpen=1
au BufReadPost *
     \ if line("'\"") > 1 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif

