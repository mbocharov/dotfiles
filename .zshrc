export ZSH="/home/mbocharov/.oh-my-zsh"
ZSH_THEME="robbyrussell"
source "$HOME/.zshrc.local"
#plugins=(git)
source $ZSH/oh-my-zsh.sh

plugins=(zsh-autosuggestions git command-time)

export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/projects
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
export VIRTUALENVWRAPPER_VIRTUALENV=/usr/bin/virtualenv
export VIRTUALENVWRAPPER_VIRTUALENV_ARGS='--no-site-packages'
export PIP_VIRTUALENV_BASE=$WORKON_HOME
export PIP_RESPECT_VIRTUALENV=true
if [[ -r `which virtualenvwrapper.sh` ]]; then
 source `which virtualenvwrapper.sh`
 #source /usr/bin/virtualenvwrapper.sh
else
 echo "WARNING: Can't find virtualenvwrapper.sh"
fi

alias vim="nvim"
alias vi="nvim"
alias oldvim="vim"
alias ap="ansible-playbook"


source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
bindkey '^ ' autosuggest-accept
